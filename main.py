# from typing import Union
from pydantic import BaseModel
from fastapi import FastAPI, status, Response
import redis
import requests
import json
import secrets
import os
app = FastAPI()

# r = redis.Redis(host='redis_1', port=6379)
# r2 = redis.Redis(host='redis_3', port=6379)
r2 = redis.Redis(host='localhost', port=6606)
r = redis.Redis(host='localhost', port=5505)

    # def __init__(self):
def loginAPI(email, password, proxy):
    try:
        if (email == ""):
            return {"msg": "Username not empty"}
        if (password == ""):
            return {"msg": "Password not empty"}
        # return userName, passWord
        url = "https://api.fshare.vn/api/user/login"
        headers = {
            "Host": "api.fshare.vn",
            "Content-Type": "application/json; charset=UTF-8",
            "Connection": "keep-alive",
            "Accept": "*/*",
            "User-Agent": "FshareMacTool-062021",
            "Accept-Language": "en-VN;q=1.0",
            "Accept-Encoding": "br;q=1.0, gzip;q=0.9, deflate;q=0.8"
        }
        userInfo = {
            "user_email": email,
            "app_key": "6SMacqqgQQ2sTh9NR4gJgHF28mADpHCL",
            "password": password
        }
        response = requests.post(url=url, headers=headers, json=userInfo, proxies={
            'http': proxy,
            'https': proxy
        })
        if response.json()['msg'] != "Login successfully!":
            return False
        r.set(email, value=json.dumps({'token': response.json()['token'], 'session_id': response.json()['session_id']}), ex=3600)
        return {'token': response.json()['token'], 'session_id': response.json()['session_id']}
    except:
        return False
    

def checkLogin(session_id, proxy):
    url = "https://api.fshare.vn/api/user/get"
    header = {
        "Host": "api.fshare.vn",
        "Accept": "*/*",
        "Cookie": "session_id=" + session_id,
        "User-Agent": "Fshare-tool-macOs/1.0 (Fshare.Fshare-tool-macOs; build:1; macOS 12.6.1) Alamofire/5.4.3",
        "Accept-Language": "en-VN;q=1.0",
        "Accept-Encoding": "br;q=1.0, gzip;q=0.9, deflate;q=0.8",
        # "Content-Type": "application/json; charset=UTF-8",
        "Connection": "keep-alive",
    }
    response = requests.get(url=url, headers=header, proxies={'http': proxy,'https': proxy})
    # print(response.json())
    try:
        if response.json()['id'] != "":
            return True
    except:
        return False
        
def getLink(session, token, password, link, proxy):
    url = "https://api.fshare.vn/api/session/download"
    # getlink.link = getlink.link.replace("/", "\/")
    header = {
        "Host": "api.fshare.vn",
        "Connection": "keep-alive",
        "Accept": "*/*",
        "Fshare-Session-id": session,
        "Accept-Language": "en-us",
        "User-Agent": "Fshare-tool-macOs/1 CFNetwork/1335.0.3 Darwin/21.6.0",
        "Content-Type": "application/json",
        "Accept-Encoding": "gzip, deflate, br"
    }
    data = {
        "token": token,
        "zipflag": "0",
        "password": password,
        "url": link
       
    }
    return requests.post(url=url, headers=header, json=data, proxies={
        'http': proxy,
        'https': proxy
    }).json()

def check_proxy(proxy):
    try:
        requests.get('https://api.myip.com/', proxies={
            'http': proxy,
            'https': proxy
        })
        return True
    except:
        return False

class Account(BaseModel):
    email: str
    password: str

class GetLink(BaseModel):
    account: Account
    url: str
    password: str | None = ""
    proxy: str
@app.post('/getlink', status_code=200)
def handle(getlink: GetLink, response: Response):
    if not check_proxy(getlink.proxy):
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {"msg": "Proxy error"}
    if not r.exists(getlink.account.email):
        if not loginAPI(getlink.account.email, getlink.account.password, getlink.proxy):
            response.status_code = status.HTTP_401_UNAUTHORIZED
            return {"error": "Email or password not correct"}
    session = json.loads(r.get(getlink.account.email))['session_id']
    token = json.loads(r.get(getlink.account.email))['token']
    if not checkLogin(session_id=session, proxy=getlink.proxy):
        r.delete(getlink.account.email)
        handle(getlink=getlink)
    location = getLink(session, token, getlink.password, getlink.url, getlink.proxy)
    try:
        response.status_code = 200
        token = secrets.token_hex(32)
        hostname = os.getenv('HOSTNAME', 'localhost:9090')
        r2.set(name=token, value=json.dumps({'location': location['location'], 'proxy': getlink.proxy}), ex=60)
        return {'location': 'http://'+ hostname + '/dll/' + token}
    except:
        response.status_code = 404
        return {'msg': location['msg']}

    

    
    # print(session)
    
    

# class LoginInfo(BaseModel):
#     email: str | None = ""
#     password: str | None = ""


# class UserInfo(BaseModel):
#     session_id: str
#     token: str


# class getLinkFshare(BaseModel):
#     session_id: str
#     token: str
#     link: str
#     password: str | None = ""


# """
# * Param: Email and Password of Fshare account
# * Return: session_id and token of account

# """


# def loginFshareAccount(userLogin):
#     if (userLogin.email == ""):
#         return {"msg": "Username not empty"}
#     if (userLogin.password == ""):
#         return {"msg": "Password not empty"}
#     # return userName, passWord
#     url = "https://api.fshare.vn/api/user/login"
#     headers = {
#         "Host": "api.fshare.vn",
#         "Content-Type": "application/json; charset=UTF-8",
#         "Connection": "keep-alive",
#         "Accept": "*/*",
#         "User-Agent": "FshareMacTool-062021",
#         "Accept-Language": "en-VN;q=1.0",
#         "Accept-Encoding": "br;q=1.0, gzip;q=0.9, deflate;q=0.8"
#     }
#     userInfo = {
#         "user_email": userLogin.email,
#         "app_key": "6SMacqqgQQ2sTh9NR4gJgHF28mADpHCL",
#         "password": userLogin.password
#     }
#     return requests.post(url=url, headers=headers, json=userInfo).json()


# # def getInfoFshareAccount():
# #     pass


# # @app.get("/")
# # def read_root():
# #     return {"Hello": "World"}

# """
# * Param: userName and passWord
# * Return: session_id and token of account
# """
# @app.post("/login")
# def loginHandle(userLogin: LoginInfo):
#     return loginFshareAccount(userLogin)

# """
# * Param: session_id and token
# * Return: Infomation of Fshare account
# """
# @app.post("/info")
# def getInfoFshareAccount(loginAccount: UserInfo):
#     url = "https://api.fshare.vn/api/user/get"
#     header = {
#         "Host": "api.fshare.vn",
#         "Accept": "*/*",
#         "Cookie": "session_id=" + loginAccount.session_id,
#         "User-Agent": "Fshare-tool-macOs/1.0 (Fshare.Fshare-tool-macOs; build:1; macOS 12.6.1) Alamofire/5.4.3",
#         "Accept-Language": "en-VN;q=1.0",
#         "Accept-Encoding": "br;q=1.0, gzip;q=0.9, deflate;q=0.8",
#         # "Content-Type": "application/json; charset=UTF-8",
#         "Connection": "keep-alive",
#     }
#     return requests.get(url=url, headers=header).json()


# @app.post("/getlink")
# def getlinks(getlink: getLinkFshare):
#     url = "https://api.fshare.vn/api/session/download"
#     # getlink.link = getlink.link.replace("/", "\/")
#     header = {
#         "Host": "api.fshare.vn",
#         "Connection": "keep-alive",
#         "Accept": "*/*",
#         "Fshare-Session-id": getlink.session_id,
#         "Accept-Language": "en-us",
#         "User-Agent": "Fshare-tool-macOs/1 CFNetwork/1335.0.3 Darwin/21.6.0",
#         "Content-Type": "application/json",
#         "Accept-Encoding": "gzip, deflate, br"
#     }
#     data = {
#         "token": getlink.token,
#         "zipflag": "0",
#         "password": getlink.password,
#         "url": getlink.link
       
#     }
#     # return header, data
#     return requests.post(url=url, headers=header, json=data).json()
